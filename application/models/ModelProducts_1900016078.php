<?php

defined('BASEPATH') or exit('No direct script access allowed');

class ModelProducts_1900016078 extends CI_Model
{
    public function get_all_data()
    {
        $this->db->select('*');
        $this->db->from('products');
        return $this->db->get()->result();
    }

    public function get_data($id_products)
    {
        $this->db->select('*');
        $this->db->from('products');
        return $this->db->get()->row();
    }

    public function add($data)
    {
        $this->db->insert('products', $data);
    }

    public function edit($data)
    {
        $this->db->where('id_products', $data['id_products']);
        $this->db->update('products', $data);
    }

    public function delete($data)
    {
        $this->db->where('id_products', $data['id_products']);
        $this->db->delete('products', $data);
    }
}
