<?php

class Products_controller_1900016078 extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        //Load Dependencies
        $this->load->model('ModelProducts_1900016078');
    }

    public function getProducts()
    {
        $data['getPoducts'] = $this->ModelProducts_1900016078->getProducts();
        $this->load->view('client', $data);
    }

    public function postProducts()
    {
        $this->form_validation->set_rules('name', 'Name', 'required');
        $this->form_validation->set_rules('price', 'Price', 'required');

        $data = array(
            'name' => $this->input->post('name'),
            'price' => $this->input->post('price'),
        );
        $this->ModelProducts_1900016078->addProducts($data);
    }

    public function deleteProducts($id_products = NULL)
    {
        $data = array('id_products' => $id_products);
        $this->ModulProducts_1900016078->deleteProducts($data);
    }

    public function putProducts($id_products = NULL)
    {
        $this->form_validation->set_rules('name', 'Name', 'required');
        $this->form_validation->set_rules('price', 'Price', 'required');

        $data = array(
            'id_products' => $id_products,
            'name' => $this->input->post('name'),
            'price' => $this->input->post('price'),
        );
        $this->ModelProducts_1900016078->updateProducts($data);
    }
}
